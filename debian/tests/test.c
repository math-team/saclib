/* Based on Figure A.1: A sample program from SACLIB User's Guide */

#include "saclib.h"

int sacMain(int argc, char **argv)
{
	Word I1,I2,I3,t;
	Word i,n;
Step1: /* Input. */
	I1 = 100;
	I2 = 100;
	n = 100;
Step2: /* Processing. */
	t = CLOCK();
	for (i=0; i<n; i++)
		I3 = IPROD(I1,I2);
	t = CLOCK() - t;
Step3:
/* Output. */
	IWRITE(I1); SWRITE(" * "); IWRITE(I2); SWRITE(" =\n"); IWRITE(I3);
	SWRITE("\nRepeating the above computation "); GWRITE(n);
	SWRITE(" times took\n"); GWRITE(t); SWRITE(" milliseconds.\n");
Return:
	return(0);
}
